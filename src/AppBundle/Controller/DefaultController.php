<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
       if( $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){

       	 return $this->redirect('/routes');
      	} else {
      	 return $this->redirect('/login');

      	}
    }
}
