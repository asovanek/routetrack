<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\RouteTrack;
class RoutesController extends Controller
{

	 /**
     * @Route("/routes", name="routes")
     */

     public function indexAction(){
     	$user=$this->container->get('security.context')->getToken()->getUser();
     	$this->data['user']=$user;

     	$repo=$this->getDoctrine()->getRepository("AppBundle:RouteTrack");

     	$result=$repo->findByUserId($user->getId());
     	$this->data['results']=$result;

     	return $this->render('AppBundle:Routes:list.html.twig',$this->data);
     }

     /**
     * @Route("/routes/add", name="add_route")
     */

     public function addAction(){

     	$this->data['user']=$this->container->get('security.context')->getToken()->getUser();

     	return $this->render('AppBundle:Routes:add.html.twig',$this->data);

     }

     /**
     * @Route("/routes/save", name="save_route")
     */

     public function saveAction(Request $r){
     	$session=new Session();
     	$name=$r->request->get('name');
     	$user=$this->container->get('security.context')->getToken()->getUser();
     	if(!empty($name)){
     	$file=$r->files->get('file');

     	if($file instanceof UploadedFile){
     		$newname=md5(time());
     		$newname=$newname.".".$file->guessExtension();
     		$dir=$this->get('kernel')->getRootDir() . '/../web/uploads';
     		try{

     			$file->move($dir,$newname);
     		}catch(FileException $f){

     			return  new Response($f);
     		}

     	$route= new RouteTrack();

     	$route->setName($name);
     	$route->setPath('/uploads'."/".$newname);
     	$route->setUserId($user->getId());
     	$m=$this->getDoctrine()->getManager();
     	$m->persist($route);
    	$m->flush();
     	}else {

     	$session->getFlashBag()->add('message','Error');
     	}
  		
     	$session->getFlashBag()->add('message','Route saved');
     	}else {
     		$session->getFlashBag()->add('message','Error');

     	}
     	return $this->redirect('/routes');

     }


}
